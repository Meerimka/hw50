class Machine {
	constructor(){
        this.isTurn = false;
	}
	turnOn(){
        this.isTurn = true;
        console.log(`Machine is Turned on!`);
	}
	turnOff(){
        this.isTurn = false;
        console.log(`Machine is Turned off!`);
	}
};
class HomeAppliance extends Machine {
    constructor() {
        super();
        this.isPlug = false;
    }
    plugIn(){
        this.isPlug = true;
        console.log(`HomeAppliance is plugged in`);
	}
	plugOff(){
        this.isPlug = false;
        console.log(`HomeAppliance is plugged off`);
	}
};

class WashingMachine extends HomeAppliance {
    constructor(){
        super();
        }
        run(){
            if(this.isPlug === true && this.isTurn === true){
                console.log('WashingMachine is ready to washing!');
            }else if (this.isPlug === false && this.isTurn === false){
                console.log('WashingMachine doesn\'t ready to wash!');
            }
        }
};

class LightSource extends HomeAppliance {
    constructor() {
        super();
        this.level = 0;
    }
    setLevel(level){
        if(level < 1  || level >100){
            console.log(`Warning! LightSource in dangerus level `)
        }else{
            this.level = level;
            console.log(`LightSource ${this.level}! `);
        }
	}
};

class AutoVehicle extends Machine {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    }
	setPosition(x,y){
            this.x = x;
            this.y = y;
		console.log(`Position coordinates  x and y are equal to ${this.x}! and ${this.y}!` );
	}
};
class Car extends AutoVehicle {
    constructor() {
        super();
        this.speed = 10;
    }
	setSpeed(speed){
		this.speed = speed;
		console.log('Speed is equal to '+ this.speed);
	}

	run(x,y){
		const interval = setInterval(()=>{
			let newX = this.x + this.speed;
			if (newX >= x){
				newX = x;
			}
			let newY = this.y + this.speed;
			if (newY >= y){
				newY = y;
			}
			this.setPosition(newX,newY);
			console.log(`Car is drived!  \nPosition is ${newX}! and ${newY}!`);

			if (newX === x && newY === y ){
				clearInterval(interval);
				console.log(`Car is stoped!`)
			};

		},1000);
	}
};


